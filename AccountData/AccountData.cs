﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using testingApp.Models;

namespace testingApp.AccountData
{
    public class AccountData
    {
        string path = @"C:\Users\Z0049VCF\source\repos\testingApp\testingApp\jsonFiles\db1.json";
        public List<Accounts> GetAccounts()
        {
            List<Accounts> ListOfAccounts = new List<Accounts>();
            var jsonString = File.ReadAllText(path);
            JObject data = JObject.Parse(jsonString);
            ListOfAccounts = data["accounts"].ToObject<List<Accounts>>();
            return ListOfAccounts;
        }
            
         public void AddAccounts(Accounts acc)
        {
            var itema = new Accounts()
            {
                name = acc.name,
                id = acc.id,
                country = acc.country,
                about = acc.about,
                userIds = acc.userIds
            };

            string item = JsonConvert.SerializeObject(itema);

            try
            {
                var json = File.ReadAllText(path);
                JObject data = JObject.Parse(json);
                var accounts = data.GetValue("accounts") as JArray;
                var newData = JObject.Parse(item);


                accounts.Add(newData);
                data["accounts"] = accounts;

                string result = JsonConvert.SerializeObject(data, Formatting.Indented);
                File.WriteAllText(path, result);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }
        public void UpdateAllTheAccountsByRemovingTheUserId(int userid)
        {

            var jsonString = File.ReadAllText(path);
            JObject data = JObject.Parse(jsonString);
            JArray accounts = (JArray)data["accounts"];
            List<Accounts> list = accounts.ToObject<List<Accounts>>();

            list.ForEach(acc =>
            {
                List<int> l= acc.userIds.ToList();
                if (l.Contains(userid))
                {
                    l.Remove(userid);
                    acc.userIds = l.ToArray();
                }
            });
            data["accounts"] = JArray.FromObject(list);

            string output = JsonConvert.SerializeObject(data, Formatting.Indented);
            
            File.WriteAllText(path,output);

        }

        public void UpdateAccount(Accounts existingAccount)
        {
            try
            {
                string json = File.ReadAllText(path);
                var jObject = JObject.Parse(json);
                JArray accounts = (JArray)jObject["accounts"];
                

                foreach (var item in accounts.Where(obj => obj["id"].Value<int>() == existingAccount.id))
                {
                    item["name"] = existingAccount.name;
                    item["country"] = existingAccount.country;
                    item["about"] = existingAccount.about;
                    item["userIds"] = JArray.FromObject(existingAccount.userIds.ToList());
                }
                
                jObject["accounts"] = accounts;

                
                
                string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);

                
                File.WriteAllText(path, output); //here is the issue!

               

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public void RemoveAccount(Accounts acc)
        {
            try
            {
                var json = File.ReadAllText(path);
                var jObject = JObject.Parse(json); 
                
                JArray accounts = (JArray)jObject["accounts"];


                var toDelete = accounts.First(obj => obj["id"].Value<int>() == acc.id);
                

                accounts.Remove(toDelete);
                jObject["accounts"] = accounts;
                
                string output = JsonConvert.SerializeObject(jObject,Formatting.Indented);
                
                File.WriteAllText(path, output);

                RemoveFromProjects(acc.id);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }

        }

        public void RemoveFromProjects(int id)
        {
            new ProjectData.ProjectData().UpdateTheAccountIdsArrayByRemovingTheAccountsFromTheProjects(id);
        }


    }
}
