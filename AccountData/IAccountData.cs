﻿
using System.Collections.Generic;

using testingApp.Models;

namespace testingApp.AccountData
{
    public interface IAccountData
    {
         List<Accounts> GetAccounts();

         Accounts GetAccountsById(int id);

         Accounts AddAccount(Accounts acc);

         void DeleteAccount(Accounts acc);

         Accounts UpdateAccounts(Accounts acc);

        List<Accounts> GetAccountsByIds(int[] accountIds);




        // account database starts

        AccountsTable AddAccountToDatabase(AccountsTable account);

        void addToTableWithAccid(int accid, int noOfUserIDs);

        void addToTableWithAccidWithUserid(int[] userIds);

        AccountsTable GetSingle(int id);

        AccountsTable UpdateAccountInTheDatabase(AccountsTable updated);

        // account database ends
    }
}
