﻿using System.Collections.Generic;
using System.Linq;
using testingApp.Models;
using Microsoft.EntityFrameworkCore;

namespace testingApp.AccountData
{
    public class MockAccountData : IAccountData
    {
        public List<Accounts> Accounts = new AccountData().GetAccounts();
        AccountData data = new AccountData();

        public RegisterContext _context;
        
        public MockAccountData(RegisterContext context)
        {
            _context = context;
        }

        public Accounts AddAccount(Accounts acc)
        {
            var item = new Accounts();
            if (this.Accounts.Count > 0)
            {
                item = this.Accounts[this.Accounts.Count - 1];
            }

            acc.id = item.id + 1;
            this.Accounts.Add(acc);
            data.AddAccounts(acc);


            return acc;

        }

        public AccountsTable AddAccountToDatabase(AccountsTable account)
        {
            //AccountsTable acc = new AccountsTable(account._name, account.id, account.country, account.about);
            _context.AccountsTable.Add(account);
            _context.SaveChanges();
            return account;
        }

        public void addToTableWithAccid(int accid, int noOfUserIDs)
        {
            List<TableWithAccid> list = new List<TableWithAccid>();
            var join_acc_with_user = _context
                .TableWithAccid
                .ToList()
                .FindLast(item => item.accid == accid - 1)
                .join_acc_with_user;

            for(int i = 1; i <= noOfUserIDs; i++)
            {
                TableWithAccid obj = new TableWithAccid
                {
                    accid = accid,
                    join_acc_with_user = join_acc_with_user + i
                    
                };
                list.Add(obj);
            }
            _context.TableWithAccid.AddRange(list);
            _context.SaveChanges();

        }

        public void addToTableWithAccidWithUserid(int[] userIds)
        {
            List<TableWithAccidWithUserId> list = new List<TableWithAccidWithUserId>();

            var join_acc_with_user = _context.
                TableWithAccidWithUserId.AsNoTracking().ToList().Count();

            foreach(int i in userIds)
            {
                TableWithAccidWithUserId obj = new TableWithAccidWithUserId
                {
                    userid = i,
                    join_acc_with_user = ++ join_acc_with_user
                };
                list.Add(obj);
            }
            _context.TableWithAccidWithUserId.AddRange(list);
            _context.SaveChanges();

        }

        public void DeleteAccount(Accounts acc)
        {
            this.Accounts.Remove(acc);
            data.RemoveAccount(acc);
        }

        public List<Accounts> GetAccounts()
        {
            return this.Accounts;
        }

        public Accounts GetAccountsById(int id)
        {
            return this.Accounts.Find(account => account.id == id);
        }

        public Accounts UpdateAccounts(Accounts acc)
        {
            var current = GetAccountsById(acc.id);
            
            current.name = acc.name;
            current.country = acc.country;
            current.about = acc.about;
            current.userIds = acc.userIds;

            data.UpdateAccount(current);

            return current;

        }

        List<Accounts> IAccountData.GetAccountsByIds(int[] accountIds)
        {
            List<int> lstArray = accountIds.ToList();
            List<Accounts> AllAccounts = GetAccounts().FindAll(item => item.id == lstArray.Find(number => number == item.id));
            return AllAccounts;
        }

        public AccountsTable GetSingle(int id)
        {
            AccountsTable item =  _context.AccountsTable
                .First
                (AccountsTable => AccountsTable.id == id);
            if (item != null)
                System.Diagnostics.Debug.WriteLine("get single called ... item updated " + item.name);
            else
                System.Diagnostics.Debug.WriteLine("not found");


            return item;
        }

        public AccountsTable UpdateAccountInTheDatabase(AccountsTable updated)
        {

            _context.AccountsTable.Update(updated);
            _context.SaveChanges();

            return updated;
        }
    }
}
