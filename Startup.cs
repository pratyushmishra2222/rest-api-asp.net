
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using testingApp.AccountData;
using testingApp.Models;
using testingApp.ProjectData;
using testingApp.UsersData;
using Microsoft.EntityFrameworkCore;


/*[assembly: OwinStartup(typeof(testingApp.Startup))] */ //step 1
namespace testingApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //step 2
        /*public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            app.UseCors(CorsOptions.AllowAll);
            var OAuthOptions = newOAuthAuthorizationServerOptions
                {
                                AllowInsecureHttp = true,
                                TokenEndpointPath = newPathString("/token"),
                                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(10),
                                Provider = newDotNetTechyAuthServerProvider()

                };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(newOAuthBearerAuthenticationOptions());


            HttpConfiguration config = newHttpConfiguration();
            WebApiConfig.Register(config);

        }*/

        /*public void Configuration(IAppBuilder app)
        {
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);

        }*/

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
           
            services.AddCors();

            //add the interface and the class implementing the interface
            //services.Add(new ServiceDescriptor(typeof(IProjectData), new MockProjectData()));
            
            services.AddScoped<IProjectData, MockProjectData>();
            services.AddScoped<IAccountData, MockAccountData>();
            services.AddScoped<IUsersData, MockUserData>();

            /* services.Add(new ServiceDescriptor(typeof(IAccountData), new MockAccountData()));*/
            //services.Add(new ServiceDescriptor(typeof(IUsersData), new MockUserData()));
            
            string mySqlConnectionStr = Configuration.GetConnectionString("DefaultConnection");
           
            services.AddDbContext<RegisterContext>(options =>
                options.UseMySql(mySqlConnectionStr, ServerVersion.AutoDetect(mySqlConnectionStr)));

           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
