﻿
namespace testingApp.Models
{
    public class Accounts
    {
        public string name { get; set; }
        public int id { get; set; }
        public string country { get; set; }
        public string about { get; set; }
        public int[] userIds { get; set; }
        
    }
}
