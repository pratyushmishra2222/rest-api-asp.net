﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("table_with_prid_with_accid")]
    public class TableWithPridWithAccid
    {
        [Key]
        public int join_pr_with_acc { get; set; }

        public int accid { get; set; }
    }
}
