﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace testingApp.Models
{
    public class RegisterContext : DbContext
    {
        public RegisterContext(DbContextOptions<RegisterContext> options) : base(options){}
        
        public DbSet<Added> Added { get; set; }
        public DbSet<Login> Login { get; set; }

        public DbSet<ProjectsTable> ProjectsTable { get; set; }

        public DbSet<AccountsTable> AccountsTable { get; set; }

        public DbSet<Users> UsersTable { get; set; }

        public DbSet<tableWithPrid> tableWithPrid { get; set; }
        public DbSet<TableWithPridWithAccid> TableWithPridWithAccid { get; set; }

        public DbSet<TableWithAccid> TableWithAccid { get; set; }
        public DbSet<TableWithAccidWithUserId> TableWithAccidWithUserId { get; set; }


    }
}
