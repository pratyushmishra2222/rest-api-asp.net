﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("users")]
    public class Users
    {
        public int id { get; set; }
        public string name { get; set; }
        public string bank { get; set; }
        public int accNo { get; set; }
        public int balance { get; set; }
    }

}
