﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace testingApp.Models
{
    [Table("register")]
    public class Added
    {
        [Key]
        public int id { get; set; }
        
        public string email { get; set; }
        
        public string firstName { get; set; }
        
        public string lastName { get; set; }
        
        public string password { get; set; }

        public string token { get; set; }
    }
}
