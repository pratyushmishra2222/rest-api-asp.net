﻿

namespace testingApp.Models
{
    class Status
    {
        public bool _isCorrect { get; set; }
        public string _WhatIsWrong { get; set; }

        public string _Token { get; set; }

        public Status(bool isCorrect, string WhatIsWrong, string Token)
        {
            _isCorrect = isCorrect;
            _WhatIsWrong = WhatIsWrong;
            _Token = Token;
        }

    }
}
