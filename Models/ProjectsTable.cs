﻿using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("Projects")]
    public class ProjectsTable
    {
        public string name { get; set; }

        public int id { get; set; }

        public string owner { get; set; }

        public string des { get; set; }

        
        public ProjectsTable(string name, int id, string owner, string des)
        {
            this.name = name;
            this.id = id;
            this.owner = owner;
            this.des = des;
        }
    }
}
