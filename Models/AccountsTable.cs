﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("accounts")]
    public class AccountsTable
    {
        public string name { get; set; }

        [Key]
        public int id { get; set; }

        public string country { get; set; }

        public string about { get; set; }

        public AccountsTable(string name,int id,string country,string about)
        {
            this.name = name;
            this.id = id;
            this.country = country;
            this.about = about;
        }
    }
}
