﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("table_with_accid")]
    public class TableWithAccid
    {
        [Key]
        public int join_acc_with_user { get; set; }

        public int accid { get; set; }

    }
}
