﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("table_with_prid")]
    public class tableWithPrid
    {
        [Key]
        public int join_pr_with_acc { get; set; }

       public int prid { get; set; }
    }
}
