﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testingApp.Models
{
    [Table("table_with_accid_with_userid")]
    public class TableWithAccidWithUserId
    {
        [Key]
        public int join_acc_with_user { get; set; }

        public int userid { get; set; }
    }
}
