﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using testingApp.helper;
using testingApp.Models;
using testingApp.UsersData;

namespace testingApp.Controllers
{
    
    [ApiController]
    public class UsersController : ControllerBase
    {

        //dependency inject the interface
        private IUsersData IUsersData;

        public UsersController(IUsersData IUsersData)
        {
            this.IUsersData = IUsersData;
        }

        [HttpGet]
        [Route("[controller]/all")]
        public List<Users> getAllUsers()
        {
            return IUsersData.GetUsersFromDatabase();
        }

        [HttpGet]
        [Route("[controller]/{id}")]
        public IActionResult getUserById(int id)
        {
            Users user = IUsersData.GetUserById(id);
            if(user == null)
            {
                return NotFound($"User with id {id} not Found");
            }
            return Ok(user);

        }
        [HttpGet("[controller]")]
        public IActionResult GetMultipleUsersAtATime()
        {

            string queryString = Request.QueryString.ToString();
            HelperClass help = new HelperClass(queryString);
            List<Users> users = IUsersData.GetUsersByIds(help.SeparateIdsFromQueryString());
            return Ok(users);
        }
        [HttpPost]
        [Route("[controller]")]
        public IActionResult Adduser(Users user)
        {

            IUsersData.AddUser(user);
            IUsersData.AddUsersToDatabase(user);

            return CreatedAtAction(nameof(Adduser), new { id = user.id }, user);

        }

        [HttpDelete]
        [Route("[controller]/{id}")]
        public IActionResult DeleteUser(int id)
        {

            Users ToDelete = IUsersData.GetUserById(id);

            if(ToDelete != null)
            {
                IUsersData.DeleteUser(ToDelete);
                return Ok();
            }
            return NotFound();
        }

        [HttpPut]
        [Route("[controller]/{id}")]
        public IActionResult UpdateUser(Users user)
        {

            /*Users ToUpdate = IUsersData.GetUserById(user.id);
            if (ToUpdate != null)
            {
                user.id = ToUpdate.id;
                //IUsersData.EditUser(user);
                IUsersData.UpdateUserInTheDatabase(user);
                return Ok();
            }*/

            // AccountsTable Table = new AccountsTable(accToUpdate.name, accToUpdate.id, accToUpdate.country, accToUpdate.about);
            Users users = user;
            if (users != null)
            {
                IUsersData.UpdateUserInTheDatabase(users);
                return Ok();
            }

            return NotFound();
        }




    }
}
