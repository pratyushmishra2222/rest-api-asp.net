﻿
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using testingApp.Models;

namespace testingApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
       
        public RegisterContext _Rcontext;
       

        public LoginController(RegisterContext Rcontext)
        {
            
            _Rcontext = Rcontext;
           
        }

        [HttpPost("userLogin")]
        public JsonResult CheckIfUserExists([FromBody]Login find)
        {

            string Token = null;
           
            if (_Rcontext.Added.ToList().Where(item => item.email == find.email && item.password == find.password).ToList().Count == 1)
            {
                List<Added> l = _Rcontext.Added.ToList().Where(item => item.email == find.email && item.password == find.password).ToList();
                return new JsonResult(new Status(true,"none", l.FirstOrDefault().token));
            }
            else if (_Rcontext.Added.ToList().Where(item => item.email == find.email && item.password != find.password).ToList().Count == 1)
            {
                return new JsonResult(new Status(false, "password",Token));
            }

            else if (_Rcontext.Added.ToList().Where(item => item.email != find.email && item.password == find.password).ToList().Count == 1)

            {
                return new JsonResult(new Status(false, "email",Token));
            }

            return new JsonResult(new Status(false, "both",Token));
        }

    }
   
}
