﻿using Microsoft.AspNetCore.Mvc;
using testingApp.Models;
using testingApp.ProjectData;
namespace testingApp.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ProjectsController : ControllerBase
    {
        private IProjectData IProjectData;

       public ProjectsController(IProjectData IProjectData)
        {
            this.IProjectData = IProjectData;
        }
        
        [HttpGet]
        [Route("")]
        public IActionResult GetProjects()
        {
            IProjectData.GetProjectsFromDatabase();//.ForEach(item => System.Diagnostics.Debug.WriteLine(item));
            return Ok(IProjectData.GetProjects());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetProjectsById(int id)
        {
            var project = IProjectData.GetSingleProject(id);
            //var project1 = IProjectData.GetSingle(id);
           
            if (project == null)
            {
                return NotFound($"Project with id {id} doesn't exist");
            }
            return Ok(project);
        }


        [HttpPost]
        [Route("")]
        public IActionResult AddProject(Projects project)
        {
            IProjectData.AddProject(project);

            ProjectsTable pr = new ProjectsTable(project.name, project.id, project.owner, project.des);

            IProjectData.AddProjectToDatabase(pr);
            IProjectData.addToTableWithPrid(project.id, project.accountIds.Length);
            IProjectData.addToTableWithPridWithAccid(project.accountIds);

            return CreatedAtAction(nameof(AddProject),new { id = project.id},project);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteProject(int id)
        {
            var ToDelete = IProjectData.GetSingleProject(id);

            if(ToDelete != null)
            {
                IProjectData.DeleteProject(ToDelete);
                return Ok();
            }

            return NotFound($"Project with id {id} doesn't exist");


        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateProject(Projects project)
        {
            /*var projectExist = IProjectData.GetSingleProject(project.id);
            if (projectExist != null)
            {
                //project.id = projectExist.id;
                IProjectData.EditProject(project);
                return Ok();
            }*/
           
            ProjectsTable pr1 = new ProjectsTable(project.name, project.id, project.owner, project.des);
            if (pr1 != null)
            {
                IProjectData.UpdateProjectsInTheDatabase(pr1);
                return Ok();
            }

            return NotFound($"Project with id {project.id} doesn't exist");


        }


    }
}
