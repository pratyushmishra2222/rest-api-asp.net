﻿using Microsoft.AspNetCore.Mvc;
using testingApp.helper;
using testingApp.Models;

namespace testingApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SignUpController : ControllerBase
    {
        public RegisterContext _context;
       

        public SignUpController(RegisterContext context)
        {
            _context = context;
           
        }

        [HttpPost("createcontact")]
        public JsonResult CreateContact([FromBody]Added add)
        {
            HelperClass _help = new HelperClass();
            add.token = _help.GenerateRandomToken();
            _context.Added.Add(add);
            _context.SaveChanges();

            return new JsonResult(add.token);
        }
    }
}
