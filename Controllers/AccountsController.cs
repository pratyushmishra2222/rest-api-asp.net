﻿
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using testingApp.AccountData;
using testingApp.helper;
using testingApp.Models;

namespace testingApp.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class AccountsController : ControllerBase
    {
        private IAccountData IAccountData;
        public AccountsController(IAccountData IAccountData)
        {
            this.IAccountData = IAccountData;
        }

        [HttpGet("{id}")]
        public IActionResult GetAccountsById(int id)
        {
            Accounts acc = IAccountData.GetAccountsById(id);
            if (acc != null)
            {
                return Ok(acc);
            }
            return NotFound($"Account with id {id} not found");
        }

        [HttpGet("")]
        public IActionResult GetMultipleAccountsAtATime()
        {

            string queryString = Request.QueryString.ToString();
            HelperClass help = new HelperClass(queryString);
            List<Accounts> acc = IAccountData.GetAccountsByIds(help.SeparateIdsFromQueryString());
            return Ok(acc);
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAllAccounts()
        {
            return Ok(IAccountData.GetAccounts());
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteAccounts(int id)
        {
            Accounts acc = IAccountData.GetAccountsById(id);
            if (acc != null)
            {
               IAccountData.DeleteAccount(acc);
               return Ok();
            }
            return NotFound();
        }

        [HttpPost]
        [Route("")]
        public void AddAccounts(AccountWithProjectId value)
        {

            //AccountWithProjectId data = JObject.FromObject(obj);
            System.Diagnostics.Debug.WriteLine("hello worlds");
            System.Diagnostics.Debug.WriteLine(value.ProjectId);
            //object obj1 = JsonConvert.DeserializeObject<AccountWithProjectId>(obj.ToString());

            //AccountWithProjectId acc = new AccountWithProjectId()
            //JsonConvert.DeserializeObject<AccountWithProjectId>(obj.ToString());
            // System.Diagnostics.Debug.WriteLine($"project id is {obj1}");

            /* IAccountData.AddAccount(accToAdd);

             AccountsTable acc = new AccountsTable(accToAdd.name, accToAdd.id, accToAdd.country, accToAdd.about);

             IAccountData.AddAccountToDatabase(acc);
             IAccountData.addToTableWithAccid(accToAdd.id, accToAdd.userIds.Length);
             IAccountData.addToTableWithAccidWithUserid(accToAdd.userIds);*/

            //add to the table named 'table_with_prid_with_accid'
            /*
                accid   join_pr_with_acc
                  3         1
                  4         2
                  5         3
                  7         4 // newly added
             */
            //add to the table named 'table_with_prid'

            /*
                prid   join_pr_with_acc
                  1         1
                  1         2
                  1         3
                  1         4 // newly added
             */


            //return CreatedAtAction(nameof(AddAccounts), new { id = accToAdd.id }, accToAdd);
        }
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateAccounts(Accounts accToUpdate)
        {

            /*var ExistOrNot = IAccountData.GetAccountsById(accToUpdate.id);
            if (ExistOrNot != null)
            {
                accToUpdate.id = ExistOrNot.id;
                IAccountData.UpdateAccounts(accToUpdate);

               return Ok();
            }*/

            AccountsTable Table = new AccountsTable(accToUpdate.name, accToUpdate.id, accToUpdate.country, accToUpdate.about);
            if (Table != null)
            {
                IAccountData.UpdateAccountInTheDatabase(Table);
                return Ok();
            }

            return NotFound();
        }
    }
}
