﻿using System.Collections.Generic;
using testingApp.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System;
using System.Linq;
using Newtonsoft.Json;

namespace testingApp.ProjectData

{
    public class ProjectData
    {
       
        string path = @"C:\Users\Z0049VCF\source\repos\testingApp\testingApp\jsonFiles\db1.json";
        public List<Projects> GetProjects()
        {
            List<Projects> ListOfProjects = new List<Projects>();
            var jsonString = File.ReadAllText(path);
            JObject data = JObject.Parse(jsonString);
            ListOfProjects = data["projects"].ToObject<List<Projects>>();
            return ListOfProjects;
        }
        public void AddProject(Projects pro)
        {

            var itema = new Projects()
            {
                name = pro.name,
                id = pro.id,
                owner = pro.owner,
                des = pro.des,
                accountIds = pro.accountIds
            };

            string item = JsonConvert.SerializeObject(itema);
            //var item = "{ \"name\": \""+pro.name+"\", \"id\": " + pro.id + ", \"owner\":\""+pro.owner+"\", \"desc\":\""+ pro.desc + "\", \"accountIds\": [" + string.Join(",", pro.accountIds) + "] }";
            try
            {
                
               
                var json = File.ReadAllText(path);
                JObject data = JObject.Parse(json);
                var projects = data.GetValue("projects") as JArray;
                var newData = JObject.Parse(item);

                
                projects.Add(newData);
                data["projects"] = projects;

                string result = JsonConvert.SerializeObject(data, Formatting.Indented);
                File.WriteAllText(path, result);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            

        }

        public void UpdateProject(Projects existingProject)
        {
            
            try
            {
                string json = File.ReadAllText(path);
                var jObject = JObject.Parse(json);
                JArray projects = (JArray)jObject["projects"];

                foreach (var item in projects.Where(obj => obj["id"].Value<int>() == existingProject.id))
                {
                    item["name"] = existingProject.name;
                    item["owner"] = existingProject.owner;
                    item["des"] = existingProject.des;
                    item["accountIds"] = JArray.FromObject(existingProject.accountIds.ToList());

                }
                jObject["projects"] = projects;
                string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                File.WriteAllText(path, output);
                
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }
        
        public void RemoveProject(Projects pr)
        {
            try
            {
                var json = File.ReadAllText(path);
                var jObject = JObject.Parse(json); // converts into key-value pair!
                
                JArray projects = (JArray)jObject["projects"];
                

                var toDelete = projects.FirstOrDefault(obj=>obj["id"].Value<int>() == pr.id);
                
                projects.Remove(toDelete);
                jObject["projects"] = projects;

                string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);
               
                File.WriteAllText(path, output);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
           
        }
        
        public void UpdateTheAccountIdsArrayByRemovingTheAccountsFromTheProjects(int accid)
        {
            var jsonString = File.ReadAllText(path);
            JObject data = JObject.Parse(jsonString);
            JArray projects = (JArray)data["projects"];
            List<Projects> list = projects.ToObject<List<Projects>>();

            list.ForEach(pro => 
            {
                List<int> l = pro.accountIds.ToList();
                if (l.Contains(accid))
                {
                    l.Remove(accid);
                    pro.accountIds = l.ToArray();
                }
            });

            data["projects"] = JArray.FromObject(list);

            string output = JsonConvert.SerializeObject(data,Formatting.Indented);
            File.WriteAllText(path,output);

        }
    }
}
