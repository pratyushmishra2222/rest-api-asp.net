﻿using System;
using System.Collections.Generic;
using testingApp.Models;

namespace testingApp.ProjectData
{
    public interface IProjectData
    {
        List<Projects> GetProjects();

        Projects GetSingleProject(int Id);

        ProjectsTable GetSingle(int Id);

       
        Projects AddProject(Projects emp);


        void DeleteProject(Projects emp);

        Projects EditProject(Projects emp);

        // project database starts
        ProjectsTable AddProjectToDatabase(ProjectsTable project);

        void addToTableWithPrid(int prid,int noOfAccountIds);
        void addToTableWithPridWithAccid(int[] accountIds);

        ProjectsTable UpdateProjectsInTheDatabase(ProjectsTable updated);


        public void GetProjectsFromDatabase();

        // project database ends



    }
}
