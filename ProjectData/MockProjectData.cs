﻿
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using testingApp.Models;

namespace testingApp.ProjectData
{
    public class MockProjectData : IProjectData
    {
        public List<Projects> Projects = new ProjectData().GetProjects();
        ProjectData data = new ProjectData();

        public RegisterContext _context;

        public MockProjectData(RegisterContext context)
        {
            _context = context;
        }

        
        public Projects AddProject(Projects pro)
        {
            var item = new Projects();
            if (this.Projects.Count > 0)
            {
                item = this.Projects[this.Projects.Count - 1];
            }

            pro.id = item.id + 1;
            this.Projects.Add(pro);
            data.AddProject(pro);
            return pro;
        }

       
        
        public void DeleteProject(Projects emp)
        {
            this.Projects.Remove(emp);
            data.RemoveProject(emp);

        }

        public Projects EditProject(Projects emp)
        {
            Projects existingProject = GetSingleProject(emp.id);
            existingProject.name = emp.name;
            existingProject.owner = emp.owner;
            existingProject.des = emp.des;
            existingProject.accountIds = emp.accountIds;

            data.UpdateProject(existingProject);
            return existingProject;
        }

        public List<Projects> GetProjects()
        {
            return this.Projects;
        }

        public Projects GetSingleProject(int Id)
        {

            return this.Projects.Find(index => index.id == Id);

        }

        public void addToTableWithPrid(int prid, int noOfAccountIds)
        {
            
            List<tableWithPrid> list = new List<tableWithPrid>();
            var join_pr_with_acc = _context.tableWithPrid.ToList().FindLast(item => item.prid == prid - 1).join_pr_with_acc;
            
            for (int i = 1; i <= noOfAccountIds; i++)
            {
                tableWithPrid obj = new tableWithPrid
                {
                    prid = prid,
                    join_pr_with_acc = join_pr_with_acc + i
                };
                list.Add(obj);
            }
            
            _context.tableWithPrid.AddRange(list);
            _context.SaveChanges();
        }

        public void addToTableWithPridWithAccid(int[] accountIds)
        {
            List<TableWithPridWithAccid> list = new List<TableWithPridWithAccid>();
            var join_pr_with_acc = _context.TableWithPridWithAccid.AsNoTracking().ToList().Count;


            foreach (int i in accountIds)
            {
                TableWithPridWithAccid obj = new TableWithPridWithAccid
                {
                    accid = i,
                    join_pr_with_acc = ++join_pr_with_acc
                };
                list.Add(obj);
            }
            _context.TableWithPridWithAccid.AddRange(list);
            _context.SaveChanges();
        }

        public ProjectsTable AddProjectToDatabase(ProjectsTable emp)
        {
            ProjectsTable pr = new ProjectsTable(emp.name, emp.id, emp.owner, emp.des);
            
            _context.ProjectsTable.Add(pr);
            _context.SaveChanges();
            return emp;
        }

        public ProjectsTable GetSingle(int Id)
        {
            return _context.ProjectsTable.First(project => project.id == Id);
        }

       public ProjectsTable UpdateProjectsInTheDatabase(ProjectsTable updated)
        {
            _context.ProjectsTable.Update(updated);
            _context.SaveChanges();

            return updated;
        }


        public void GetProjectsFromDatabase()
        {

            //return _context.ProjectsTable.FromSqlRaw(query).ToList<Projects>();*/

            string query = "select name,id,owner,des,GROUP_CONCAT(table_with_prid_with_accid.accid)" 
                            + "as accountIds from projects join table_with_prid on"
                            + "projects.id = table_with_prid.prid natural join" 
                            + "table_with_prid_with_accid group by table_with_prid.prid";

            /*var item = from projectTable in _context.ProjectsTable
                    join tableWithPrid in _context.tableWithPrid
                    on projectTable.id equals tableWithPrid.prid
                    join TableWithPridWithAccid in _context.TableWithPridWithAccid*/

            var projects = _context.ProjectsTable.FromSqlRaw(query);
            //System.Diagnostics.Debug.WriteLine(projects.ToList().ForEach(item=>item.acc));
            //return projects.ToList<Projects>();
            //selct .join.join try using this.
        }
    }

    
}
