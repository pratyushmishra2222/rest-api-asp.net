﻿
using testingApp.Models;

namespace testingApp.ProjectData
{
    public class DbProjectData : IDbProjectData
    {
        private RegisterContext _context;

        public DbProjectData()
        {
        }

        public DbProjectData(RegisterContext context)
        {
            _context = context;
        }


        public ProjectsTable AddProjectToDatabase(ProjectsTable emp)
        {
            ProjectsTable pr = new ProjectsTable(emp.name, emp.id,emp.owner, emp.des);
            _context.ProjectsTable.Add(pr);
            _context.SaveChanges();
            return emp;
        }

    }
}
