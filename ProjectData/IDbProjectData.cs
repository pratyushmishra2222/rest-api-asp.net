﻿
using testingApp.Models;

namespace testingApp.ProjectData
{
    public interface IDbProjectData
    {
        ProjectsTable AddProjectToDatabase(ProjectsTable emp);
    }
}
