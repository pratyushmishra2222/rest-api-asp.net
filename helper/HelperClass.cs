﻿using System;
using System.Linq;

namespace testingApp.helper
{
    public class HelperClass
    {
        private string queryString;
        public HelperClass(string queryString)
        {
            this.queryString = queryString;
        }

        public HelperClass(){}

        public int[] SeparateIdsFromQueryString()
        {
            string[] separator = { "?id=", "&id=" };
            string[] values = (this.queryString.Split(separator,
                                            StringSplitOptions.RemoveEmptyEntries));
            
            int[] ids = new int[values.Length];
            
            for(int i = 0; i < values.Length; i++)
            {
               ids[i] = (int.Parse(values[i]));
            }
           
            return ids;
        }

        public string GenerateRandomToken()
        {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return token;
        }
    }
}
