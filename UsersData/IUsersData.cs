﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testingApp.Models;

namespace testingApp.UsersData
{
    public interface IUsersData
    {
        List<Users> GetUsers();

        List<Users> GetUsersFromDatabase();

        Users GetUserById(int id);

        Users AddUser(Users User);

        Users EditUser(Users user);

        void DeleteUser(Users user);

        List<Users> GetUsersByIds(int[] userIds);

        Users AddUsersToDatabase(Users user);

        Users UpdateUserInTheDatabase(Users user);


    }
}
