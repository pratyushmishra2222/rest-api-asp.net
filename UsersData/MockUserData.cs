﻿using System;
using System.Collections.Generic;
using System.Linq;
using testingApp.Models;

namespace testingApp.UsersData
{
    public class MockUserData : IUsersData
    {
        public List<Users> ListOfUsers = new UserData().GetList();
        UserData data = new UserData();

        public RegisterContext _context;

        public MockUserData(RegisterContext context)
        {
            _context = context;
        }
        
        public Users AddUser(Users User)
        {
            var item = new Users();
            if (ListOfUsers.Count > 0)
            {
                item = ListOfUsers[ListOfUsers.Count - 1];
            }

            User.id = item.id + 1;
            ListOfUsers.Add(User);
            data.AddUser(User);
            return User;
        }

        public Users AddUsersToDatabase(Users user)
        {
            _context.UsersTable.Add(user);
            _context.SaveChanges();
            return user;

        }

        public void DeleteUser(Users user)
        {
            this.ListOfUsers.Remove(user);
            data.RemoveUser(user);
        }

        public Users EditUser(Users user)
        {
            Users ToEdit = GetUserById(user.id);

            ToEdit.name = user.name;
            ToEdit.bank = user.bank;
            ToEdit.balance = user.balance;
            ToEdit.accNo = user.accNo;
            data.UpdateUser(ToEdit);
            return ToEdit;
        }

        public Users GetUserById(int id)
        {
            return _context.UsersTable.ToList().Find(user => user.id == id);
        }

        public List<Users> GetUsers()
        {
            return _context.UsersTable.ToList();
        }

        public List<Users> GetUsersByIds(int[] userIds)
        {
            List<int> lstArray = userIds.ToList();
            //List<Users> AllUsers = GetUsers().FindAll(item => item.id == lstArray.Find(number => number == item.id));
            List<Users> AllUsers = GetUsersFromDatabase().FindAll(item => item.id == lstArray.Find(number => number == item.id));
            return AllUsers;
        }

        public Users UpdateUserInTheDatabase(Users user)
        {
            _context.UsersTable.Update(user);
            _context.SaveChanges();
            return user;
        }

        public List<Users> GetUsersFromDatabase()
        {
           return _context.UsersTable.ToList();
        }
    }
}
