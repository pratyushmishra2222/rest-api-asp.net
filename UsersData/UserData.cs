﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using testingApp.Models;

namespace testingApp.UsersData
{
    public class UserData
    {
        string path = @"C:\Users\Z0049VCF\source\repos\testingApp\testingApp\jsonFiles\db1.json";
        public List<Users> GetList()
        {
            List<Users> ListOfUsers = new List<Users>();
            var jsonString = File.ReadAllText(path);
            JObject data = JObject.Parse(jsonString);
            ListOfUsers = data["users"].ToObject<List<Users>>();
            return ListOfUsers;
        }

        public void AddUser(Users user)
        {

            var itema = new Users()
            {
                id = user.id,
                name = user.name,
                bank = user.bank,
                balance = user.balance,
                accNo = user.accNo
            };

            string item = JsonConvert.SerializeObject(itema);

           try
            {
                var json = File.ReadAllText(path);
                JObject data = JObject.Parse(json);
                var users = data.GetValue("users") as JArray;
                var newData = JObject.Parse(item);
                
                users.Add(newData);
                data["users"] = users;

                string result = JsonConvert.SerializeObject(data, Formatting.Indented);
                File.WriteAllText(path, result);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        public void UpdateUser(Users existingUser)
        {

            try
            {
                string json = File.ReadAllText(path);
                var jObject = JObject.Parse(json);
                JArray users = (JArray)jObject["users"];

                foreach (var item in users.Where(obj => obj["id"].Value<int>() == existingUser.id))
                {
                    item["name"] = existingUser.name;
                    item["bank"] = existingUser.bank;
                    item["accno"] = existingUser.accNo;
                    item["balance"] = existingUser.balance;
                }
                jObject["users"] = users;
                string output =JsonConvert.SerializeObject(jObject, Formatting.Indented);
                File.WriteAllText(path, output);

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public void RemoveFromAccounts(int id)
        {
            new AccountData.AccountData().UpdateAllTheAccountsByRemovingTheUserId(id);
        }

        public void RemoveUser(Users user)
        {
            try
            {
                var json = File.ReadAllText(path);
                var jObject = JObject.Parse(json);

                JArray users = (JArray)jObject["users"];

                var toDelete = users.First(obj => obj["id"].Value<int>() == user.id);

                users.Remove(toDelete);
                jObject["users"] = users;

                string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);

                File.WriteAllText(path, output);

                RemoveFromAccounts(user.id);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }

        }
    }
}
